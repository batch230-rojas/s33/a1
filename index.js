fetch("https://jsonplaceholder.typicode.com/todos")
.then(res => console.log(res.json()) );


fetch("https://jsonplaceholder.typicode.com/todos")
.then(res => console.log(res.status))

fetch("https://jsonplaceholder.typicode.com/todos")
.then(res => res.json()) 
.then(response => console.log(response));


fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => {

	let list = json.map((todos => {
		return todos.title;
	}))

	console.log(list);
});




fetch("https://jsonplaceholder.typicode.com/todos/1")
.then(res => res.json()) 
.then(response => console.log(response))


fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => {

	let title = json.title;
	let completed = json.completed

	console.log("The item " + title + " on the list has a status of " + completed);

});


fetch("https://jsonplaceholder.typicode.com/todos", 
	{
		method: "POST",
		headers: {
			"Content-Type" : "application/json"
		},
		body: JSON.stringify({
			completed: false,
			id: 201,
			title: "Created To Do List Item",
			userId: 1
		})
	})
	.then(response => response.json())
	.then(json => console.log(json))



	fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PUT",
	headers: {
		"Content-Type" : "application/json"
	},
	body:JSON.stringify({
		dateCompleted: "Pending",
		description: "To update the my to do list with a different structure",
		id: 1,
		status: "Pending",
		title: "Updated To Do List Item",
		userId: 1
	})
})
.then(response => response.json())
.then(json => console.log(json))


fetch("https://jsonplaceholder.typicode.com/todos/1", {
	method: "PATCH",
	headers: {
		"Content-Type" : "application/json"
	},
	body: JSON.stringify({
		dateCompleted: "07/09/21",
		status: "Complete",
	})
})
.then(response => response.json())
.then(json => console.log(json))


fetch("https://jsonplaceholder.typicode.com/todos/1", 
	{
		method: "DELETE"
	}	
)
.then(response => response.json())
.then(json => console.log(json))